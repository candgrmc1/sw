"use strict"
class Log {
    
    constructor({source="app"}){
        this.source= source
        this.FgRed = "\x1b[31m"
        this.FgGreen = "\x1b[32m"
        this.FgYellow = "\x1b[33m"
        this.FgCyan = "\x1b[36m"
        this.FgWhite = "\x1b[37m"
    }

    success(message){
        message = typeof message == 'object' ? JSON.stringify(message) : message
        console.log(this.FgGreen, `${this.source}::${message}`)
    }
    trace(message){
        message = typeof message == 'object' ? JSON.stringify(message) : message
        // console.trace(message)
        console.log(this.FgWhite,`${this.source}::${message}`)
    }
    info(message){
        message = typeof message == 'object' ? JSON.stringify(message) : message
        // console.info(message)
        console.log(this.FgCyan, `${this.source}::${message}`)
    }
    warn(message){
        message = typeof message == 'object' ? JSON.stringify(message) : message
        // console.warn(message)
        console.log(this.FgYellow, `${this.source}::${message}`)
    }
    error(message){
        message = typeof message == 'object' ? JSON.stringify(message) : message
        // console.error(message)
        console.log(this.FgRed, `${this.source}::${message}`)
    }


}

module.exports = Log