"use strict"
const moment = require('moment')
const log = require('./Log')
function verifyRequest({body,parameters}){
    const logger = new log({source:'request-validation'})
    for (let param in parameters){
       
        if(param == 'filterParams' || (parameters.filterParams && parameters.filterParams.includes(param) && body[param] == undefined)) continue

        if(parameters[param].required && body[param] == undefined){
            throw new Error('request-validation::missed-required-parameters')
        }

        switch(parameters[param].type){
            case 'date':
                if(verifyDateFormat({
                    date:body[param],
                    format:parameters[param].format
                }) == false){
                    throw new Error('request-validation::invalid-date-format')
                }
                break;
            case 'string':
                if(typeof body[param] != 'string'){
                    throw new Error('request-validation::invalid-format-string')
                }
                break;
            case 'integer':
                if(typeof body[param] != 'number'){
                    throw new Error('request-validation::invalid-format-integer')
                }
                break;
            case 'boolean':
                if(typeof body[param] != 'boolean'){
                    throw new Error('request-validation::invalid-format-boolean')
                }
                break
            
            default:
                break;

        }
        
    }

    logger.success('verified')
}

function verifyDateFormat({date,format}){
    return moment(date, format, true).isValid()
}



module.exports = {
    verifyRequest,
    verifyDateFormat,
    log
}