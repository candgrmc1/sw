module.exports = {
    development:{
        mongoDB: 'mongodb://demo:demo123@ds239578.mlab.com:39578/swenson-demo',
        SUCCESS_CODE: 0,
        SUCCESS_MSG: 'success',
        port: 8080
    },

    production: {
        mongoDB: 'mongodb://demo:demo123@ds239578.mlab.com:39578/swenson-demo',
        SUCCESS_CODE: 0,
        SUCCESS_MSG: 'success',
        port: 8000
    },
    test:{
        host: `http://localhost:8080`
    }
}