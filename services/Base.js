"use strict"
const mongoose = require('mongoose')

class Base {
    extractBody(body){
        const {page, perPage} = body;
        delete body.page;
        delete body.perPage;
        return {body,perPage,page}
    }
}

module.exports = {Base}