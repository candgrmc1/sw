"use strict"
const {
    Machine,
    Pod
} = require('../schemas')
const {
    Base
} = require('./Base')
class getProductService extends Base {
    called() {
        this.log.success('called')
    }
    async getCoffeeMachines(params) {
        const {
            body,
            page,
            perPage
        } = this.extractBody(params)
        const skip = (page - 1) * perPage
        const response = await Machine.find(body).select({
            code: 1,
            type: 1,
            waterLine: 1
        }).limit(perPage).skip(skip).lean()
        return response
    }

    async getCoffeePods(params) {
        const {
            body,
            page,
            perPage
        } = this.extractBody(params)
        const skip = (page - 1) * perPage
        const response = await Pod.find(body).select({
            code: 1,
            type: 1,
            coffeeFlavor: 1,
            packSize: 1
        }).limit(perPage).skip(skip).lean()
        return response
    }

}
getProductService.prototype.log = null;
getProductService.prototype.config = null;

module.exports = {
    getProductService
}