"use strict"
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const machineSchema = new Schema({
    _id: {
        type: String,
    },
    code:{
        type: String
    },
    type: {
        type: String
    },
    waterLine: {
        type: Boolean
    },
    createdAt: {
        type: Date
    }
}, {
    timestamps: true
});
machineSchema.index({
    code:1,
    type: 1,
    waterLine: 1
})
const Machine = mongoose.model('machines', machineSchema);

module.exports = Machine