"use strict"
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const podSchema = new Schema({
    _id: {
        type: String,
    },
    type: {
        type: String
    },
    coffeeFlavor: {
        type: String
    },
    code:{
        type: String
    },
    packSize: {
        type: Number
    },
    createdAt: {
        type: Date
    }
}, {
    timestamps: true
});

podSchema.index({
    code:1,
    type: 1,
    coffeeFlavor: 1,
    packSize: 1
})
const Pod = mongoose.model('pods', podSchema);

module.exports = Pod