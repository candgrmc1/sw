"use strict"
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const mongoose = require('mongoose')
const router = express.Router()
const env = process.env.NODE_ENV || 'development';
const config = require('./config')[env]
const port = process.env.PORT || config.port;
const services = require('./services')
app.use(express.json())
app.use(bodyParser.urlencoded({
    extended: true
}));
mongoose.connect(config.mongoDB, {
    useNewUrlParser: true
}).then(() => {
    const apiRouter = require('./routes/api')({
        router,
        services,
        config
    })
    app.get('/', (req, res) => res.send('Hello world'))
    app.use('/api', apiRouter);
});






app.listen(port)