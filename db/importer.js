const fs = require('fs')
const {
    Machine,
    Pod
} = require('../schemas')
const env = process.env.NODE_ENV || 'development';
const config = require('../config')[env]
const mongoose = require('mongoose')
async function run() {
    await mongoose.connect(config.mongoDB, {
        useNewUrlParser: true
    })
    
    await dropCollections();
    const lines = await fs.readFileSync(__dirname + '/db.txt').toString().split('\n')
    lines.pop()
    const machines = [];
    const pods = [];
    console.log('inserting..')
    for (let line of lines) {
        let parsed = line.split('–')
        let id = parsed[0].trim()

        arr = parsed[1].split(',')

        if (arr[0].includes('MACHINE')) {
            let waterLine = arr[2] && arr[2].indexOf('compatible') > -1? true : false
                machines.push({
                code: id,
                type: arr[0].trim(),
                waterLine,
                createdAt: new Date().valueOf()
            })
        } else {
            pods.push({
                code: id,
                type: arr[0].trim(),
                packSize: parseInt(arr[1]),
                coffeeFlavor: arr[2].trim(),
                createdAt: new Date().valueOf()
            })
        }

    }
    try {

        await Machine.insertMany(machines)
        await Pod.insertMany(pods)
        process.exit();
    } catch (err) {
        console.log(err)
        process.exit();
    }
}

async function dropCollections() {
    const collections = await mongoose.connection.db.collections();
    console.log('removing collection')
    for (let collection of collections) {
        try {
            await mongoose.connection.collection('machines').drop()
            await mongoose.connection.collection('pods').drop()
            return
        } catch (err) {
            console.log(err)
            return
        }
    }

}
run().then()