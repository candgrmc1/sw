"use strict"
const {
    verifyRequest,
    log
} = require('../../common')

module.exports = ({
    router,
    services,
    config
}) => {
    router.use(function (req, res, next) {
        next()
    })
    router.get('/',(req,res) =>{ res.send('Hello world')})

    router.post('/:route',async (req,res,next)=>{
        let instance = require(`./${req.params.route}`)
        try{
            // throws error if not verified
            await verifyRequest({parameters:instance.prototype.parameters,body:req.body})
            // sets reponse index
            const responseIndex = instance.prototype.responseIndex
            // handles props of the instance and related services
            instance = await handle({instance,services,route:req.params.route})

            const response = await instance.run(req.body) 
            const r = {
                code: config.SUCCESS_CODE,
                msg: config.SUCCESS_MSG,
                contentLength: response.length
            }

            // sets reponse data
            r[responseIndex] = response
            res.status(200).json(r)

        }catch(error){
            console.log(error)
            res.status(500).json({
                code: instance.prototype.faults[error.message]  || 'invalid-error',
                msg: instance.prototype.faults[error.message] ? error.message : `Please report it. ( message: ${error.message} )`
            })
        }
        
    })


    async function handle({instance,services,route}){
        console.log(instance)
        let protos = Object.getOwnPropertyNames(instance.prototype)
        for (let i of protos ){
            try{
                switch(i){
                    case 'config':
                        instance.prototype.config = config;
                        break
                    case 'run':
                    case 'constructor':
                    case 'faults':
                    case 'responseIndex':
                    case 'parameters':{
                        break;
                    } 
                    case 'log':
                        instance.prototype.log = new log({source:route})
                        break;       
                    default:
                        let s = services[i]
                        s.prototype.log = new log({source:i})
                        s.prototype.config = config

                        s = new services[i]
                        await s.called();
                        
                        instance.prototype[i] = s
                              
                }
            }catch(error){
                throw new Error(error)
            }
            

        }
        const inst = new instance()
        return inst;
    }


    return router
}

