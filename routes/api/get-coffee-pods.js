"use strict"
class getCoffeePods {
    async run(body) {
        const records = await this.getProductService.getCoffeePods(body);
        return records
    }
}

getCoffeePods.prototype.config = null
getCoffeePods.prototype.log = null
getCoffeePods.prototype.getProductService = null;
getCoffeePods.prototype.parameters = {
    type: {
        required: false,
        type: 'string'
    },
    coffeeFlavor: {
        required: false,
        type: 'string'
    },
    packSize: {
        required: false,
        type: 'integer'
    },
    page: {
        required: true,
        type: 'integer'
    },
    perPage: {
        required: true,
        type: 'integer'
    },
    filterParams: ["type", "coffeeFlavor", "packSize"]
}
getCoffeePods.prototype.faults = {
    'request-validation::missed-required-parameters': 1,
    'request-validation::invalid-date-format': 2,
    'request-validation::invalid-format-string': 3,
    'request-validation::invalid-format-integer': 4,
    'request-validation::invalid-format-boolean': 5
}
getCoffeePods.prototype.responseIndex = 'records';
module.exports = getCoffeePods