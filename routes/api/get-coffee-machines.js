"use strict"
class getCoffeeMachines {
    async run(body) {

        const records = await this.getProductService.getCoffeeMachines(body);
        this.log.success(records)
        return records
    }
}

getCoffeeMachines.prototype.config = null
getCoffeeMachines.prototype.log = null
getCoffeeMachines.prototype.getProductService = null;
getCoffeeMachines.prototype.parameters = {
    type: {
        required: false,
        type: 'string'
    },
    waterLine: {
        required: false,
        type: 'boolean'
    },
    page: {
        required: true,
        type: 'integer'
    },
    perPage: {
        required: true,
        type: 'integer'
    },
    filterParams: ["type", "waterLine"]
}
getCoffeeMachines.prototype.faults = {
    'request-validation::missed-required-parameters': 1,
    'request-validation::invalid-date-format': 2,
    'request-validation::invalid-format-string': 3,
    'request-validation::invalid-format-integer': 4,
    'request-validation::invalid-format-boolean': 5
}
getCoffeeMachines.prototype.responseIndex = 'records';
module.exports = getCoffeeMachines