const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const config = require('../config')['test']

describe('Post -> /api/get-coffee-machines', () => {
    const host = (process.env.IP) ? "http://" + process.env.IP + ':' + process.env.PORT : config.host;
    const path = '/api/get-coffee-machines'
    it('should get coffee machines with type COFFEE_MACHINE_LARGE', function (done) {
        chai
            .request(host)
            .post(path)
            // .field('myparam' , 'test')
            .set('content-type', 'application/json')
            .send({
                "type": "COFFEE_MACHINE_LARGE",
                "page": 1,
                "perPage": 10
            })
            .end(function (error, response,body) {
                console.log(response.body)
                if (error || response.body.code != 0 || response.status != 200) {
                    
                    done(body);
                } else {
                    done();
                }
            });
    });
    it('should get coffee machines with type COFFEE_MACHINE_SMALL', function (done) {
        chai
            .request(host)
            .post(path)
            // .field('myparam' , 'test')
            .set('content-type', 'application/json')
            .send({
                "type": "COFFEE_MACHINE_SMALL",
                "page": 1,
                "perPage": 10
            })
            .end(function (error, response,body) {
                console.log(response.body)
                if (error || response.body.code != 0 || response.status != 200) {
                    
                    done(body);
                } else {
                    done();
                }
            });
    });
    
    
})

describe('Post -> /api/get-coffee-pods', () => {
    const host = (process.env.IP) ? "http://" + process.env.IP + ':' + process.env.PORT : config.host;
    const path = '/api/get-coffee-pods'
    it('should get coffee pods with type COFFEE_POD_LARGE', function (done) {
        chai
            .request(host)
            .post(path)
            // .field('myparam' , 'test')
            .set('content-type', 'application/json')
            .send({
                "type": "COFFEE_MACHINE_LARGE",
                "page": 1,
                "perPage": 10
            })
            .end(function (error, response,body) {
                console.log(response.body)
                if (error || response.body.code != 0 || response.status != 200) {
                    
                    done(body);
                } else {
                    done();
                }
            });
    });
    it('should get coffee pods with type COFFEE_POD_SMALL', function (done) {
        chai
            .request(host)
            .post(path)
            // .field('myparam' , 'test')
            .set('content-type', 'application/json')
            .send({
                "type": "COFFEE_POD_SMALL",
                "page": 1,
                "perPage": 10
            })
            .end(function (error, response,body) {
                console.log(response.body)
                if (error || response.body.code != 0 || response.status != 200) {
                    
                    done(body);
                } else {
                    done();
                }
            });
    });

    it('should get coffee pods with type COFFEE_POD_SMALL and coffeeFlavor COFFEE_FLAVOR_VANILLA', function (done) {
        chai
            .request(host)
            .post(path)
            // .field('myparam' , 'test')
            .set('content-type', 'application/json')
            .send({
                "type": "COFFEE_POD_SMALL",
                "coffeeFlavor":"COFFEE_FLAVOR_VANILLA",
                "page": 1,
                "perPage": 10
            })
            .end(function (error, response,body) {
                console.log(response.body)
                if (error || response.body.code != 0 || response.status != 200) {
                    
                    done(response.body);
                } else {
                    done();
                }
            });
    });
    
    
})